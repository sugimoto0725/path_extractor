//
// Created by sugitake on 18/05/25.
//

#ifndef PROJECT_PATH_EXTRACT_NODE_H
#define PROJECT_PATH_EXTRACT_NODE_H
#include "Eigen/Core"
#include "Eigen/Geometry"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"
#include "kbys_msgs/AnglePotentialArrayStamped.h"
#include "kbys_msgs/Float64ArrayStamped.h"
#include "kbys_msgs/Float64Stamped.h"
#include "kbys_msgs/PoseTwistWithCovariance.h"
#include "kbys_msgs/PoseTwistWithCovarianceArrayStamped.h"
#include "kbys_msgs/PoseWithTwistArrayStamped.h"
#include "nav_msgs/Odometry.h"
#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/String.h"
#include "tmc_navigation_msgs/PathWithDirection.h"
#include <fstream>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/time_synchronizer.h>
#include <ros/package.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#define _USE_MATH_DEFINES
#include "matplotlibcpp.h"
#include <math.h>
#include <omp.h>


class PathExtractor
{
private:
    //ros handler
    ros::NodeHandle nh_;
    ros::Publisher angle_pub_;
    ros::Publisher potential_pub_;
    ros::Publisher maximum_info_pub_;

    //time watch for pulish header
    ros::Time time_watch_;

    //publish object
    kbys_msgs::Float64ArrayStamped candidate_angle_;
    kbys_msgs::Float64ArrayStamped candidate_potential_;
    kbys_msgs::AnglePotentialArrayStamped maximum_;

    std::vector<double> path_potential_;
    int potential_size_;

    //tf
    Eigen::Matrix4d tf_matrix_;
    std::string base_name_;
    std::string target_name_;

    //human pos and vel vector
    Eigen::Vector4d human_pos_in_robot_;
    Eigen::Vector2d polar_coor_;  //[0: distanceR, 1: angleT]
    Eigen::Vector4d human_vel_in_human_;

    //direction(goal) angle
    double direction_angle_;


public:

    ros::NodeHandle* getNodeHandlePtr();
    //コンストラクタ
    PathExtractor();
    // コールバック関数
    void callback(const kbys_msgs::PoseWithTwistArrayStampedConstPtr& human, const geometry_msgs::PoseStampedConstPtr& robot, const sensor_msgs::LaserScanConstPtr& wall_laser_points, const geometry_msgs::PoseStampedConstPtr& goal_position);
    // 座標変換行列を取得
    void getTfMatrix();
    // ロボット座標系での進行方向（ゴール方向）を取得
    void getDirectionAngle(const geometry_msgs::PoseStampedConstPtr& goal_position, Eigen::Matrix4d& tf_matrix);
    // 人の位置をメンバに格納
    void setHumanPosInVector(const kbys_msgs::PoseWithTwistArrayStampedConstPtr& human, int human_index);
    // 人の位置を極座標系に変換
    void getPolarVector();
    // 符号込みの人速度情報を取得
    void getHumanPosInWorld(const kbys_msgs::PoseTwistWithCovarianceArrayStampedConstPtr& human, Eigen::Vector4d& position, const int index);
    // ロボット座標系において向き（符号）込みの情報を取得
    void getVelInHuman(const kbys_msgs::PoseWithTwistArrayStampedConstPtr& human, const geometry_msgs::PoseStampedConstPtr& robot, const int index);
    // 速度に関するポテンシャルの算出
    void getHumanPotentialVector(std::vector<double>& human_potential_vec, const sensor_msgs::LaserScanConstPtr& wall_laser_points);
    // 壁のポテンシャルとの重ねあわせ（流れシンプル化している間は不要?）
    void getPotentialAmount(std::vector<double>& tmp_human_potential_vec);
    // グラフの描画
    void drawCostGraph(const sensor_msgs::LaserScanConstPtr& wall_laser_points);
    // 極小値（選択進行方向）の抽出
    void getPathAngle(const sensor_msgs::LaserScanConstPtr& wall_laser_points);
    // トピックをパブリッシュ
    void publish();
};
#endif //PROJECT_PATH_EXTRACT_NODE_H
