#include "path_extract_node.h"

PathExtractor::PathExtractor() : potential_size_(180), base_name_("base_link"), target_name_("map")
{
  angle_pub_ = nh_.advertise<kbys_msgs::Float64ArrayStamped>("/velocity_mode_angle", 1);
  potential_pub_ = nh_.advertise<kbys_msgs::Float64ArrayStamped>("/velocity_mode_potential", 1);
  maximum_info_pub_ = nh_.advertise<kbys_msgs::AnglePotentialArrayStamped>("/velocity_mode_maximum", 1);
  time_watch_ = ros::Time::now();
}

ros::NodeHandle* PathExtractor::getNodeHandlePtr() { return &nh_; }

void PathExtractor::callback(const kbys_msgs::PoseWithTwistArrayStampedConstPtr& human, const geometry_msgs::PoseStampedConstPtr& robot, const sensor_msgs::LaserScanConstPtr& wall_laser_points, const geometry_msgs::PoseStampedConstPtr& goal_position)
{
//  ROS_INFO("callback has called!! ");
  ros::Time start_time = ros::Time::now(); // 1周期毎のタイムウォッチ
  void getTfMatrix();
  getDirectionAngle(goal_position, tf_matrix_);  //ゴール方向の取得
  //人間の一次元ポテンシャルの取得

  for (int human_index = 0; human_index < human->states.size(); human_index++)
  {
    std::vector<double> tmp_human_potential_vec; //
    setHumanPosInVector(human, human_index);
    getPolarVector();
    getVelInHuman(human, robot, human_index);
    getHumanPotentialVector(tmp_human_potential_vec, wall_laser_points);
//    std::cout<<"size: "<<tmp_human_potential_vec.size()<<std::endl;
    getPotentialAmount(tmp_human_potential_vec);
  }
  if(path_potential_.size()==0){
    path_potential_.resize(static_cast<int>((wall_laser_points->angle_max - wall_laser_points->angle_min) / wall_laser_points->angle_increment + 1));
  }

  getPathAngle(wall_laser_points);
  candidate_angle_.header=wall_laser_points->header;
  candidate_potential_.header=wall_laser_points->header;
  maximum_.header=wall_laser_points->header;
  publish();

//今の設定でグラフの描画すると0.1ｓかかる
//ポテンシャルグラフの描画
//  drawCostGraph(wall_laser_points);
//  matplotlibcpp::clf();

  path_potential_.clear();
  candidate_angle_.data.clear();
  candidate_potential_.data.clear();
}

void PathExtractor::getTfMatrix() {
  static tf::TransformListener tf_listener;
  tf::StampedTransform transform;
  ros::Time now = ros::Time(0);
  try
  {
    tf_listener.waitForTransform(base_name_, target_name_, now, ros::Duration(1.0));
    tf_listener.lookupTransform(base_name_, target_name_, now, transform);
  }
  catch (tf::TransformException ex)
  {
    ROS_ERROR("%s", ex.what());
    return;
  }
  tf::Matrix3x3 rot_wtor(transform.getRotation());
  tf::Vector3 vec_wtor = transform.getOrigin();
  tf_matrix_ = Eigen::Matrix4d::Identity();
  {
    Eigen::Matrix3d mat_tmp;
    tf::matrixTFToEigen(rot_wtor, mat_tmp);
    tf_matrix_.block(0, 0, 3, 3) = mat_tmp;
  }
  {
    Eigen::Vector3d vec_tmp;
    tf::vectorTFToEigen(vec_wtor, vec_tmp);
    tf_matrix_.block(0, 3, 3, 1) = vec_tmp;
  }
}

//ゴール方向の取得
void PathExtractor::getDirectionAngle(const geometry_msgs::PoseStampedConstPtr& goal_position, Eigen::Matrix4d& tf_matrix)
{
  Eigen::Vector4d goal_in_world; //goal vector in world coordinate
  Eigen::Vector4d goal_in_robot; //goal vector in robot coordinate
  goal_in_world[0] = goal_position->pose.position.x;
  goal_in_world[1] = goal_position->pose.position.y;
  goal_in_world[2] = goal_position->pose.position.z;
  goal_in_world[3] = 1;
  goal_in_robot = tf_matrix * goal_in_world;
  direction_angle_ = atan2(goal_in_robot[1], goal_in_robot[0]);
}

void PathExtractor::setHumanPosInVector(const kbys_msgs::PoseWithTwistArrayStampedConstPtr &human, int human_index) {
  human_pos_in_robot_(0) = human->states[human_index].position.x;
  human_pos_in_robot_(1) = human->states[human_index].position.y;
  human_pos_in_robot_(2) = human->states[human_index].position.z;
  human_pos_in_robot_(3) = 1;
}

void PathExtractor::getHumanPosInWorld(const kbys_msgs::PoseTwistWithCovarianceArrayStampedConstPtr& human, Eigen::Vector4d& position, const int index)
{
  position(0) = human->states[index].position.x;
  position(1) = human->states[index].position.y;
  position(2) = human->states[index].position.z;
  position(3) = 1;
}

//極座標系に変換
void PathExtractor::getPolarVector()
{
  polar_coor_(0) = sqrt(pow(human_pos_in_robot_(0), 2) + pow(human_pos_in_robot_(1), 2));
  polar_coor_(1) = atan2(human_pos_in_robot_(1), human_pos_in_robot_(0));
}

//ロボット座標系において向き（符号）込みの情報を取得
void PathExtractor::getVelInHuman(const kbys_msgs::PoseWithTwistArrayStampedConstPtr& human, const geometry_msgs::PoseStampedConstPtr& robot, const int index)
{
  Eigen::Vector2d vec_human_to_robot;
  Eigen::Vector2d human_vel_in_robot;
  vec_human_to_robot(0) = -human->states[index].position.x;
  vec_human_to_robot(1) = -human->states[index].position.y;
  human_vel_in_robot(0) = human->states[index].twist.linear.x;
  human_vel_in_robot(0) = human->states[index].twist.linear.x;
  human_vel_in_robot(1) = human->states[index].twist.linear.y;
  double length_c = sqrt(pow(human->states[index].twist.linear.x, 2) + pow(human->states[index].twist.linear.y, 2));
  double length_ctor = sqrt(pow(vec_human_to_robot(0), 2) + pow(vec_human_to_robot(1), 2));
  human_vel_in_human_(0) = human_vel_in_robot.dot(vec_human_to_robot) / length_ctor;
  double length_human_vel = human_vel_in_human_(0);
  double theta = acos(length_human_vel / length_c);
  human_vel_in_human_(1) = length_c * sin(theta);
}


void PathExtractor::getHumanPotentialVector(std::vector<double>& human_potential_vec, const sensor_msgs::LaserScanConstPtr& wall_laser_points)
{
  ///旧パラメータ20181107///
//  double rad_increment, variance, A = 500, alfa = 0.05 /*0.06*/;
  int angle_range = static_cast<int>((wall_laser_points->angle_max - wall_laser_points->angle_min) / wall_laser_points->angle_increment + 1);
  human_potential_vec.resize(angle_range);

  ///新パラメータ20181107//
//  double a=0.75;
//  double distance_function=1/(polar_coor_(0)-a);
//  double velocity_rate=20; //シミュレータ上のロボットの速度は実速度の何分の一か？
//  if(polar_coor_(0)<1.0){
//    distance_function=4.0;
//  }
//  distance_function=(human_vel_in_human_(0)*velocity_rate+0.1)*distance_function;

  double a=1.0;
  double thr=1.0;
  double par=1.0;
  double velocity_rate=1; //シミュレータ上のロボットの速度は実速度の何分の一か？
  double distance_function=exp(-a*(polar_coor_(0)-par));
  distance_function=(human_vel_in_human_(0)*velocity_rate+0.1)*distance_function;
  if(distance_function>(human_vel_in_human_(0)*velocity_rate+0.1)*thr && human_vel_in_human_(0)*velocity_rate+0.1>0){
    distance_function=(human_vel_in_human_(0)*velocity_rate+0.1)*thr;
  }
  else if(distance_function<(human_vel_in_human_(0)*velocity_rate+0.1)*thr && human_vel_in_human_(0)*velocity_rate+0.1<0){
    distance_function=(human_vel_in_human_(0)*velocity_rate+0.1)*thr;
  }


  double B=0.1;
  double par2=2;
  double C=0.093;
  double thr2=0.25;
  double alpha=pow(exp(B*polar_coor_(0)-par2)+C,2);
  if(alpha<pow(thr2,2)){
    alpha=pow(thr2,2);
  }

  int i = 0;
#pragma omp parallel for
  for (double angle = wall_laser_points->angle_min; angle < wall_laser_points->angle_max; angle += wall_laser_points->angle_increment)
  {
//    human_potential_vec[i] = A * human_vel_in_human_(0) * exp(-1 * (pow(angle - polar_coor_(1), 2)) / alfa) / pow(polar_coor_(0) + 1, 2);
    human_potential_vec[i] = distance_function * exp(-1 * (pow(angle - polar_coor_(1), 2)) / (2*alpha)) ;
    i++;
  }
}

void PathExtractor::getPotentialAmount(std::vector<double>& tmp_human_potential_vec)
{
  std::vector<double>::iterator itr;
  if (path_potential_.size() != tmp_human_potential_vec.size())
  {
    path_potential_.resize(tmp_human_potential_vec.size());
  }
  std::transform(tmp_human_potential_vec.begin(), tmp_human_potential_vec.end(), path_potential_.begin(), path_potential_.begin(), std::plus<double>());
}

void PathExtractor::drawCostGraph(const sensor_msgs::LaserScanConstPtr& wall_laser_points)
{
  std::vector<double> theta_vec;
  std::vector<double> potential;
  theta_vec.resize(wall_laser_points->ranges.size());
  potential.resize(path_potential_.size());
  for (int i = 0; i < wall_laser_points->ranges.size(); i++)
  {
    theta_vec[i] = (wall_laser_points->angle_min + wall_laser_points->angle_increment * i) * 180 / M_PI;
  }
  //ポテンシャルの左右反転（視覚的に左右わかりやすくするため）
  for (int j = 0; j < path_potential_.size(); ++j)
  {
    potential[j] = path_potential_[(path_potential_.size() - 1) - j];
  }

  std::vector<double> angle_vec;
  std::vector<double> potential_sup_vec;
  if (candidate_angle_.data.size() == 1)
  {
    angle_vec.resize(2);
    potential_sup_vec.resize(2);
    angle_vec[0] = -candidate_angle_.data[0] * 180 / M_PI;
    angle_vec[1] = -candidate_angle_.data[0] * 180 / M_PI + 0.1;
    potential_sup_vec[0] = candidate_potential_.data[0];
    potential_sup_vec[1] = candidate_potential_.data[0] + 1000;
  }
  else
  {
    angle_vec.resize(candidate_angle_.data.size());
    for (int j = 0; j < candidate_angle_.data.size(); ++j)
    {
      angle_vec[j] = -candidate_angle_.data[j] * 180 / M_PI;
    }
  }

  std::vector<double> goal_angle_vec;
  goal_angle_vec.resize(2);
  goal_angle_vec[0] = -direction_angle_ * 180 / M_PI;
  goal_angle_vec[1] = -(direction_angle_ + 0.1) * 180 / M_PI;
  std::vector<double> goal_h_potential_vec;
  goal_h_potential_vec.resize(2);
  goal_h_potential_vec[0] = -1000;
  goal_h_potential_vec[1] = 1000;

  matplotlibcpp::named_plot("velocity_potential", theta_vec, potential, "-r");
  //  matplotlibcpp::named_plot("velocity_potential", theta_vec, path_potential,
  //  "-r");

  if (candidate_angle_.data.size() == 1)
  {
    matplotlibcpp::named_plot("vel_candidate_angle", angle_vec,
                              potential_sup_vec, "-b");
  }
  else
  {
    matplotlibcpp::named_plot("vel_candidate_angle", angle_vec,
                              candidate_potential_.data, "-b");
  }
  matplotlibcpp::named_plot("goal_angle", goal_angle_vec, goal_h_potential_vec,
                            "-g");
  matplotlibcpp::ylim(-0.2, 0.2);
  matplotlibcpp::legend();
  matplotlibcpp::draw();
  matplotlibcpp::pause(0.001);
}


void PathExtractor::getPathAngle(const sensor_msgs::LaserScanConstPtr& wall_laser_points)
{
  double laser_range = 90;//[degree]
  kbys_msgs::Float64ArrayStamped path_angle;
  int min_num = 0;
  int max_num = 0;
  //  double offset_param = M_PI * 10.0 / 180;
  double min = 100;
  int i = 1;
  for (double angle = wall_laser_points->angle_min; angle < wall_laser_points->angle_max; angle += wall_laser_points->angle_increment)
  {
    if(angle<-laser_range*M_PI/180||angle>laser_range*M_PI/180) {
      i++;
      continue;
    }
    if (path_potential_[i - 1] > path_potential_[i] and path_potential_[i + 1] > path_potential_[i] and path_potential_[i] < 0)
    {
      candidate_angle_.data.resize(min_num + 1);
      candidate_potential_.data.resize(min_num + 1);
      candidate_angle_.data[min_num] = angle;
//      if(angle*180/M_PI>120)std::cout<<"angle: "<<angle*180/M_PI<<std::endl;
//      if(angle*180/M_PI<-120)std::cout<<"angle: "<<angle*180/M_PI<<std::endl;
//      else std::cout<<"within angle: "<<angle*180/M_PI<<std::endl;
      candidate_potential_.data[min_num] = path_potential_[i];
      min_num++;
    }
    if (path_potential_[i - 1] < path_potential_[i] and path_potential_[i + 1] < path_potential_[i])
    {
      maximum_.adata.resize(max_num+1);
      maximum_.pdata.resize(max_num+1);
      maximum_.adata[max_num] = angle;
      maximum_.pdata[max_num] = path_potential_[i];
      max_num++;
    }
    i++;
  }
}

void PathExtractor::publish()
{
//  std::cout << "rate is:" << ros::Time::now() - time_watch_ << "\n";

  maximum_info_pub_.publish(maximum_);
  angle_pub_.publish(candidate_angle_);
  potential_pub_.publish(candidate_potential_);
};

int main(int argc, char** argv)
{
  ROS_INFO("node has started");
  ros::init(argc, argv, "path_extract");
  PathExtractor extractor;
  ros::NodeHandle* nh_ = extractor.getNodeHandlePtr();
  message_filters::Subscriber<kbys_msgs::PoseWithTwistArrayStamped> human_sub(*nh_, "/emulated_human_velocities", 1);
  message_filters::Subscriber<geometry_msgs::PoseStamped> robot_sub(*nh_, "/global_pose", 1);
  message_filters::Subscriber<sensor_msgs::LaserScan> laser_points_sub(*nh_, "/hsrb/base_scan", 1);
//  message_filters::Subscriber<sensor_msgs::LaserScan> laser_points_sub(*nh_, "/laser_scan/static_wall", 1);
  message_filters::Subscriber<geometry_msgs::PoseStamped> goal_position_sub(*nh_, "/goal_position", 1);
  typedef message_filters::sync_policies::ApproximateTime<kbys_msgs::PoseWithTwistArrayStamped, geometry_msgs::PoseStamped, sensor_msgs::LaserScan, geometry_msgs::PoseStamped> MySyncPolicy;
  message_filters::Synchronizer<MySyncPolicy> sync(MySyncPolicy(300), human_sub, robot_sub, laser_points_sub, goal_position_sub);  //
  sync.registerCallback(boost::bind(&PathExtractor::callback, &extractor, _1, _2, _3, _4));
  matplotlibcpp::figure();
  ros::spin();
  return 0;
}
